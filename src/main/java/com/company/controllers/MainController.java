package com.company.controllers;

import com.company.entities.Email;
import com.company.entities.User;
import com.company.repositories.EmailRepository;
import com.company.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

@Controller
public class MainController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EmailRepository emailRepository;

    @RequestMapping(value = "/")
    public String getIndex(Model model){
        model.addAttribute("email", new Email());
        model.addAttribute("userForm", new User());
//        model.addAttribute("action_url","/main/index/user");
//        model.addAttribute("action_rl","/main/index/email");
        return "main/index";
    }

    @RequestMapping(value = "/main/index/email", method = RequestMethod.POST)
    @ResponseBody
    public String postEmail(Email email){
        email.setDescription("shunaqa");
        emailRepository.save(email);
        return "redirect:" + "/";
    }

    @RequestMapping(value = "/main/index/user", method = RequestMethod.POST)
    @ResponseBody
    public String postUser(User user){
        user.setDate(new Date());
        userRepository.save(user);
        return "redirect:/main/index";
    }



}
