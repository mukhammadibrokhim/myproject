package com.company.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

        httpSecurity.authorizeRequests().anyRequest().permitAll()
                .and().csrf().disable();

//        httpSecurity
//                .authorizeRequests()
//                     .antMatchers("/").not().fullyAuthenticated()
//                     .antMatchers("/admin").anonymous()
//                     .antMatchers("/webjars/**").permitAll()
//                     .antMatchers("/css/**").permitAll()
//                     .antMatchers("/js/**").permitAll()
//                     .antMatchers("/images/**").permitAll()
//                .anyRequest().authenticated()
//                .and()
//                    .formLogin()
//                    .loginPage("/login")
//                    .permitAll()
//                .and()
//                    .logout()
//                    .permitAll()
//                    .logoutSuccessUrl("/");


    }
}
